/*
 * Project clientside webscripten: Bergrouteplanner
 *
 *
 * @author Geert Ariën <geert.arien@kahosl.be>
 */




/*********************************************
 * GLOBAL VARIABLES
 *********************************************/

// a global variable for the map object
var map;
// an array for collecting all the markers
var allMarkers = [];
// a globar variable for the polyline
var line;
// a global variable for the ElevationService
var elevator;





/*********************************************
 * CODE TO EXCECUTE WHEN THE PAGE IS LOADED
 *********************************************/

$(document).ready(function() {

  // set the options for the map
  var mapOptions = {
    zoom: 14,
    center: new google.maps.LatLng(46.442, 10.622),
    streetViewControl: false,
    scaleControl: true,
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };
        
  // initialise a new google maps object
  map = new google.maps.Map($('#mapCanvas').get(0), mapOptions);

  // initialise an google maps elevation service object
  elevator = new google.maps.ElevationService();

  // append span for distance indicator
  $('#mapCanvas').append('<span id="distance">0.0 km</span>');

  // add an event listener for adding a marker on left click
  google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(event.latLng, true);
    $('#noRoute').remove();
  });

  // add watermark to the input field
  $('#inputName').watermark('Geef uw route een naam', {
    useNative: false
  });

  // remove the no title error when the user enters new input
  $('#inputName').on('input', function() {
    $('#noTitle').remove();
  })

  // prevent default action of all links
  $('a').on('click', function(e) {
    e.preventDefault();
  });

  // add event for adding the route to the database
  $('#save').on('click', function() {
    // check the markers and input before inserting the route
    if (allMarkers.length < 2 && $('#noRoute').length == 0) {
      $('#errors').append('<li id="noRoute">Geen route uitgestippeld</li>');
    }
    if ($('#inputName').val().trim() == '' && $('#noTitle').length == 0) {
      $('#errors').append('<li id="noTitle">Geen titel opgegeven</li>');
    }
    // check if there are any errors
    if ($('#errors li').length == 0) {
      addRoute();
      $(this).hide();
      $('#reset').hide();
      $('#newRouteTut').hide();
      $('#newRoute').show();
      $('#inputName').val('').hide();
    }
  });

  // add event for resetting all markers and input field
  $('#reset').on('click', function() {
    resetMarkers();
    $('#inputName').val('');
    $('#distance').text('0.0 km');
  });

  // add event for changing the content of the webpage when a user wants to plot a new route
  $('#newRoute').on('click', function() {
    configureGui('write');
  });

  // add events to the route list-items, to load the selected route
  $('#routes ul li').on('click', function() {
    loadRoute(this);
  });  
});





/*********************************************
 * FUNCTIONS TO MANIPULATE A ROUTE
 *********************************************/ 

// load a route from the database
var loadRoute = function(caller) {
    var routeId = $(caller).attr('data-id');
    $.ajax({
      url: 'api.php',
      type: 'get',
      dataType: 'json',
      data: {
      action: 'route',
      id: routeId
      },
      success: function(data, textStatus, jqXHR) {
        resetMarkers(data.route.markers);
        setPictures(allMarkers[0].position);
        configureGui('read');
        // set the text of the title, prevent javascript and html injection
        $('#routeName h2').text(data.route.title);
        $(caller).addClass('selected');
        setChart();
        map.setCenter(allMarkers[0].position);
      },
      error : function(jqXHR, textStatus, errorThrown) {
      }
     });
   }


// add a route to the database
var addRoute = function() {
  var markerString = [];

  // get the lat & lng coördinates from each marker and add them to an array
  for (var i = 0; i < allMarkers.length; i++) {
    markerString.push({ "lat" : allMarkers[i].position.lat(), "lng" : allMarkers[i].position.lng()});
  }

  var routeTitle = $('#inputName').val();

  $.ajax({
    url: 'api.php',
    type: 'post',
    dataType: 'json',
    data: {
      action: 'add',
      title: routeTitle,
      markers: markerString
    },
    // on successfully adding the route to the database, add the route to the list and load the route
    success: function(data, textStatus, jqXHR) {   
      setPictures(allMarkers[0].position);    
      $('#routes ul').append('<li data-id="' + data.id + '" class="item"><p></p></li>');
      var addedRoute = $('#routes ul li[data-id=' + data.id + ']');
      // set the text value of the list item to prevent javascript and html injection
      addedRoute.children('p').text(routeTitle);
      addedRoute.on('click', function(e) {
        loadRoute(this);
      });
      configureGui('read');
      // remove event listeners for each marker
      for (var i = 0; i < allMarkers.length; i++) {
        google.maps.event.clearInstanceListeners(allMarkers[i]);
        allMarkers[i].setDraggable(false);
      }
      $('#routeName h2').text(routeTitle);
      $(addedRoute).addClass('selected');
      setChart();
    },
    error : function(jqXHR, textStatus, errorThrown) {
    }
  });
}





/*********************************************
 * FUNCTIONS FOR MANIPULATING THE CHART
 *********************************************/


// load the chart with all of the data
var setChart = function() {

  var locations = [];
  var chartNumbers = [];
  var distanceTraveled = 0;

  // get the locations from all the markers and put them in an array
  for (var i = 0; i < allMarkers.length; i++) {
    locations.push(allMarkers[i].position);
  }

  // Create a LocationElevationRequest object using the array's values
  var positionalRequest = {
    'locations': locations
  }

  // Initiate the location request
  elevator.getElevationForLocations(positionalRequest, function(results, status) {
    if (status == google.maps.ElevationStatus.OK) {

      if (results.length > 0) {
        var elevations = [];

        // put the elevation and distance in an array and put the elevations in an array
        chartNumbers.push([0, results[0].elevation]);
        elevations = [results[0].elevation];
        for (var i = 1; i < results.length; i++) {
          distanceTraveled += google.maps.geometry.spherical.computeDistanceBetween(allMarkers[i - 1].position, allMarkers[i].position);
          chartNumbers.push([distanceTraveled, results[i].elevation]);
          elevations.push(results[i].elevation);
        }

        // plot the chart & set the tooltip's content
        plotChart(chartNumbers);
        setChartTooltip(elevations);       
      } else {
        alert('No results found');
      }
    } else {
      alert('Elevation service failed due to: ' + status);
    }
  });
}


// draw the chart with the data
var plotChart = function(chartNumbers) {

  // if there already exists another chart, destroy it first
  if ($('#chartCanvas').data('jqplot')) {
    $('#chartCanvas').data('jqplot').destroy();
  }

  $('#chartCanvas').show().jqplot([chartNumbers], {
    title: 'Elevatieprofiel',
    seriesDefaults: {
      color: '#179191',
      fill: true,
      fillAndStroke: true,
      fillColor: '#4bb2c5',
      pointLabels: { 
      show: true, 
      location: 's', 
      ypadding: 10 }
    },
    axes:{
      xaxis:{
        label:'Afstand (meters)',
      },
      yaxis:{
        label:'Hoogte (meters)'
      }
    },
    highlighter: {
      show: true,
      sizeAdjust: 7.5,
      tooltipAxes: 'y',
      tooltipLocation: 'ne'
    }
  });

  // set css for title and axis labels
  var styles = {
    'bottom' : '5px',
    'font-size' : '12px',
    'color' : '#232323'   
  }
  $('.jqplot-xaxis-label').css(styles);
  styles = {
    'left' : '5px',
    'font-size' : '12px',
    'color' : '#232323'
  }
  $('.jqplot-yaxis-label').css(styles);
  styles = {
    'top' :'3px',
    'color' : '#000',
    'font-weight' : 'bold'
  }
  $('.jqplot-title').css(styles);
}


// set the tooltip data of the chart
var setChartTooltip = function(elevations) {
  // get the maximum and minimum from the elevations array
  var max = Math.max.apply(Math, elevations);
  var min = Math.min.apply(Math, elevations);

  // add tooltip icon
  $('#chartCanvas').append('<a href ="#" class="tooltip"><img src="img/icons/info.png" alt="icon comments"></a>');

  // add span element for tooltip 
  $('#chartCanvas a.tooltip').append('<span id="chartTooltip"></span>').on('click', function(e) {
    e.preventDefault();
  });

  // set the content of the tooltip
  $('#chartTooltip').html(
    'Max. hoogte: ' + max.toFixed(2) + ' m'
    + '<br>Min. hoogte: ' + min.toFixed(2) + ' m'
    + '<br>Hoogteverschil: ' + (max - min).toFixed(2) + ' m'
    + '<br>Totale afstand: ' + (getDistance() * 1000).toFixed(2) + ' m');       
}





/*********************************************
 * MISCELLANEOUS FUNCTIONS
 *********************************************/


// place a marker and draw a line from the last marker to the new one
var placeMarker = function(latLng, editable) {
  // check if the marker should be editable or not
  var marker = new google.maps.Marker({
    position: latLng, 
    map: map,
    draggable: editable,
  });

  // make the first marker green
  if (allMarkers.length == 0) {
    marker.setIcon('http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png');
  }

  // draw a line if there is another marker
  if (!line) {
    line = new google.maps.Polyline({
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });
    line.setMap(map);
  }
  marker.linePosition = line.getPath().length;
  line.getPath().push(marker.position);

  // make the marker editable and calculate the distance if the user plotting a new route
  if (editable) {
    $('#distance').text(getDistance().toFixed(1) + ' km');

    google.maps.event.addListener(marker, 'drag', function(e) { 
      line.getPath().setAt(marker.linePosition, e.latLng);
      $('#distance').text(getDistance().toFixed(1) + ' km');
    }); 

    google.maps.event.addListener(marker, 'rightclick', function(e) { 
      line.getPath().removeAt(marker.linePosition);
      var indexMarker = allMarkers.indexOf(marker);
      allMarkers.splice(indexMarker, 1);
      marker.setMap(null);
      for (var i = indexMarker; i < allMarkers.length; i++ ) {
        allMarkers[i].linePosition--;
      }
      $('#distance').text(getDistance().toFixed(1) + ' km');
      if (indexMarker == 0 && allMarkers.length > 0) {
        allMarkers[0].setIcon('http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png');
      }
    }); 

    // in opera and chrome the browser would crash if a user right clicks on a marker while the input field had focus (bug)
    // this quick fix prevents this bug by blurring the input field on a marker mouseover
    google.maps.event.addListener(marker, 'mouseover', function(e) {
      $('#inputName').blur();
    });
  }
  // push the new marker to the array of markers
  allMarkers.push(marker);
}


// reset all the marker and load new markers if they are supplied
var resetMarkers = function(positions) {
  for (var i = 0; i < allMarkers.length; i++) {
    allMarkers[i].setMap(null);
  }
  allMarkers.length = 0;
  if (line) {
    line.setMap(null);
    line = null;
  }
  if (positions) {
    for (var i = 0; i < positions.length; i++) {
      placeMarker(new google.maps.LatLng(positions[i]['lat'], positions[i]['lng']), false);
    }
  }
}


 // search the flickr api for pictures from the surrounding area and add them to the "pics" div
var setPictures = function(position) {
  $('#pics').empty().show();
  var url = "http://api.flickr.com/services/rest/?method=flickr.photos.search&safe_search=1&extras=url_s&per_page=10&page=1&format=json&nojsoncallback=1";
  url += '&api_key=45a0ccef95c767c46edaed583488911e';
  url += '&lat=' + position.lat();
  url += '&lon=' + position.lng();
  url += '&radius=2';
  $.ajax({
    url: url,
    success: function(data) {
      for (var i = 0; i < data.photos.photo.length; i++) {
        var photo = data.photos.photo[i];
        // check if the image has the correct dimensions and allow only 3 pictures to be added
        if (photo.height_s >= 150 && photo.height_s <= 180 && photo.width_s == 240 && $('#pics img').length < 3) {
          $('#pics').append($('<img width="158" class="flickr"/>').attr('src', photo.url_s));
        }
      }            
    }
  });
}

// configure the layout of the page depending on the mode
var configureGui = function(mode) {
  switch (mode) {
    case 'read':
      $('.item').removeClass('selected');
      $('#save').hide();
      $('#reset').hide();
      $('#newRouteTut').hide();
      $('#newRoute').show();
      $('#inputName').val('').hide();
      $('#mapCanvas').css('border-color', '#3AB4B4');
      $('footer').css('border-color', '#3AB4B4');
      $('#routeName').empty().append('<h2></h2>').show();
      // set the distance on the distance indicator
      $('#distance').text(getDistance().toFixed(1) + ' km')
      // remove all event listeners from the map object so the user can't manipulate the route
      google.maps.event.clearInstanceListeners(map);
      break;
    case 'write':
      $('#newRoute').hide();
      resetMarkers();
      $('#distance').text('0.0 km');
      $('.item').removeClass('selected');
      $('#chartCanvas').hide();
      $('#newRouteTut').show();
      $('#save').show();
      $('#reset').show();
      $('#inputName').val('').show();
      $('#pics').empty().hide();
      $('#mapCanvas').css('border-color', '#A61B1B');
      $('footer').css('border-color', '#A61B1B');
      $('#routeName h2').hide();
      $('#routeName').empty().hide();
      // add event listener for adding a mark on left click
      google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng, true);
        $('#noRoute').remove();
      });
      break;
  }
}


// get the current distance of the polyline
var getDistance = function() {
  return (google.maps.geometry.spherical.computeLength(line.getPath()) / 1000);
}
