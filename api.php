<?php

	// includes
	require_once __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'datalayer.php';

	// datalayer
	$db = new DataLayer();

	// get vars
	$action = isset($_GET['action']) ? $_GET['action'] : '';

	// 'all' action; usage: call api.php?action=all
	if ($action == 'all') {		
		// get routes
		$routes = $db->getRoutes();

		// send to client
		exit(json_encode(array(				
			'routes' => $routes,
			'status' => 200,				
			'message' => 'fetch OK'
		)));
	}

	// handle POST
	if (isset($_POST['title']) && isset($_POST['markers'])) {
	  // build new route
	  $route = array('title' => $_POST['title'], 'markers' => $_POST['markers']);
	  // write to csv
	  $id = $db->insertRoute($route);
	  // exit 
	  exit(json_encode(array(
	  		'id' => $id,
			'status' => 200,
			'message' => 'add ok'
		)));
	}

	// get a route
	if ($action == 'route') {

		// check if the id exists
		if ($db->existsRoute($_GET['id'])) {
			// delete contact
			$route = $db->getRoute($_GET['id']);

			// send to client
			exit(json_encode(array(
				'route' => $route,
				'status' => 200,
				'message' => 'fetch OK'
				)));
			}
		else {
			// send to client with error code
			http_response_code(404);
			exit(json_encode(array(
				'status' => 404,
				'message' => 'Route not found'
				)));
		}
	}


	// invalid action
	http_response_code(400);
	exit(json_encode(array(
		'status' => 400,				
		'message' => 'invalid API call'
	)));		

