<?php

  // includes
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'template.php';
  require_once __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'datalayer.php';

  // datalayer
  $db = new DataLayer();

  // retrieve routes
  $routes = $db->getRoutes();

  // build template
  $tpl = new Template(__DIR__  . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'index.tpl');

  // parse routes
  $tpl->setIteration('iRoutes');
  foreach ($routes as $route) {
    $tpl->assignIteration('id', $route['id']);
    $tpl->assignIteration('title', $route['title']);
    $tpl->refillIteration();
  }
  $tpl->parseIteration('iRoutes');

  // display
  $tpl->display();


?>