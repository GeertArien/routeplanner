<?php

	/**
	* Includes
	* ----------------------------------------------------------------
	*/

	// includes
	require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php';
	require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .  'functions.php';

	/**
	* DataLayer Class
	* Based on the datalayer class from "Clientside Webscripten: Opgave 5"
	*
	* @author    Geert Ariën <geert.arien@kahosl.be>
	*/

	class DataLayer {

		/**
		 * Database connection handler
		 */
		var $link;

		/**
		 * Class constructor.
		 */
		public function __construct() {
			$this->link = @mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or showError(mysqli_connect_error($this->link), 'connect');
		}


		/**
		 * Checks whether a route exists or not
		 *
		 * @param	int 		$id Id of the route
		 * @return	bool
		 */
		public function existsRoute($id) {
			// build query
			$query = sprintf('SELECT * FROM routes WHERE id = %d',
				(int) $id
			);

			// execute query
			$result = @mysqli_query($this->link, $query) or showError(mysqli_error($this->link), 'query');

			// return result
			return mysqli_num_rows($result) == 1;
		}

		/**
		 * Get a route by id
		 *
		 * @param	int 		$id Id of the route
		 * @return	array
		 */
		public function getRoute($id) {
			// build query
			$query = sprintf('SELECT * FROM routes WHERE id = %d',
				(int) $id
			);

			// execute query
			$result = @mysqli_query($this->link, $query) or showError(mysqli_error($this->link), 'query');

			// result found?
			if (!($record = @mysqli_fetch_array($result))) showError('route with id ' . $id . ' not found', 'query');

			// return result
			return array('id' => $record['id'], 'title' => $record['title'], 'markers' => json_decode($record['markers']));
		}

		/**
		 * Get all routes
		 *
		 * @return	array
		 */
		public function getRoutes() {
			// results array
			$routes = array();

			// build query
			$query = sprintf('SELECT id, title FROM routes');

			// execute query
			$result = @mysqli_query($this->link, $query) or showError(mysqli_error($this->link), 'query');

			// add results
			while ($record = mysqli_fetch_array($result)) {
				$routes[] = array('id' => $record['id'], 'title' => $record['title']);
			}

			// return results
			return $routes;
		}

		/**
		 * Insert a route
		 *
		 * @param	Route 		$route The route to insert
		 * @return	int 		the id of the route inserted
		 */
		public function insertRoute($route) {

			// build query
			$query = sprintf('INSERT INTO routes (title, markers) VALUES ("%s", "%s")',
				mysqli_real_escape_string($this->link, $route['title']),
				mysqli_real_escape_string($this->link, json_encode($route['markers']))
			);

			// execute query
			@mysqli_query($this->link, $query) or showError(mysqli_error($this->link), 'query');

			// return id
			return mysqli_insert_id($this->link);
		}
	}

// EOF
