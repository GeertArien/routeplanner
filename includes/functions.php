<?php

	/**
	 * Handles error messages
	 *
	 * @param string $message
	 * @param string $type
	 * @return void
	 */
	function showError($message, $type = 'general') {
		// log error
		file_put_contents(ERROR_LOG, '[' . strtoupper($type) .'] ' . date("Y-m-d H:i:s") . ' ' . $message . "\n", FILE_APPEND);

		// debug mode: show the error
		if (DEBUG === true) {
			die($message);
		} 

		// halt script
		exit();
	}

// EOF