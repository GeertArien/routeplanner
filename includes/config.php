<?php

/**
 * Config - Debug
 * ----------------------------------------------------------------
 */

	// Is debug enabled? If so, errors are shown on screen
	define('DEBUG', false);

	// Error log
	define ('ERROR_LOG', 'files/errorlog.txt');

/**
 * Config - Database Config
 * ----------------------------------------------------------------
 */

	//Database Server Host
	define ('DB_HOST', 'localhost');

	// Database Server Username
	define ('DB_USER', 'root');

	// Database Server Password
	define ('DB_PASS', 'root');

	// Database Name
	define ('DB_NAME', 'routeplanner');

// EOF
