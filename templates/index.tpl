<!DOCTYPE html>
<html>
  <head>
    <title>Bergrouteplanner</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css" />
  </head>

  <body>

    <header>
      <h1><a href="#">Bergrouteplanner</a></h1>
    </header>

    <div id="wrapper" class="clearfix">  

      <div id="details">
        <div id="routeName">
        </div>
        <div id="newRouteTut" class="clearfix">
          <p>
            Plaats markers op de map om een route te plannen. Wanneer je klaar bent met de route te plannen kan je de route een naam geven en opslaan.
          </p>
          <div id="controls">
            <ul>
              <li><b>Linker klik:</b> marker plaatsen</li>
              <li><b>Rechter klik:</b> marker verwijderen</li>
              <li><b>Slepen:</b> marker verplaatsen</li>
            </ul>
          </div>
        </div>
        <div id="mapCanvas"></div>
        <div id="chartCanvas" class="tooltip"></div>
        <div id="pics"></div>
      </div>      

      <div id="navigation">
        <div id="routeControls">
          <h3>Nieuwe Route</h3>
          <p id="distance"></p>
          <input type="text" id="inputName" name="inputName" autocomplete="off" maxlength="45">
          <a href="" id="save">Opslaan</a>
          <a href="" id="reset">Resetten</a>
          <a href="" id="newRoute">Aanmaken</a>
          <ul id="errors"></ul>
        </div>
        <div id="routes">
          <h3>Routes</h3>
          <ul>
{iteration:iRoutes}
            <li data-id="{$id|htmlentities}" class="item"><p>{$title|htmlentities}</p></li>
{/iteration:iRoutes}
          </ul>
        </div>
      </div>

    </div>

    <footer>
      <p>Clientside Webscripting 1, 2013</p>
    </footer>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyArWJXAd55kqd3Q18HHCaO2Y7TRcKuxO78&libraries=geometry&sensor=false"></script>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/jquery.jqplot.min.js"></script>
    <script src="js/plugins/jqplot.highlighter.min.js"></script>
    <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="excanvas.js"></script><![endif]-->
    <script src="js/plugins/jquery.watermark.min.js"></script>
    <script src='js/scripts.js'></script>

  </body>

</html>